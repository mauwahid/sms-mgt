package com.infomedia.sms.service;

import com.infomedia.sms.domain.Client;
import com.infomedia.sms.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public void save(String name, String clientTypeId, int billingType,
                     String address, String city, String postal,
                     String phone, String email, int status){

        Client client = new Client();
        client.setName(name);
        client.setClientTypeId(clientTypeId);
        client.setBillingType(billingType);
        client.setAddress(address);
        client.setCity(city);
        client.setPostal(postal);
        client.setPhone(phone);
        client.setEmail(email);
        client.setStatusId(status);

        clientRepository.save(client);

    }

    public Iterable<Client> getAllData(){
        return clientRepository.findAll();
    }
}
