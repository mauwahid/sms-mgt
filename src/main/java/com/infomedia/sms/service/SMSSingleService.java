package com.infomedia.sms.service;

import com.infomedia.sms.domain.Sms;
import com.infomedia.sms.repository.SMSRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SMSSingleService {

    @Autowired
    private SMSRepository smsRepository;

    public String saveSMS(String msisdn, String message,
                         String senderId, String requestBy,
                         String scheduled){

        Sms sms = new Sms();
        sms.setMsisdn(msisdn);
        sms.setMessage(message);
        sms.setSenderId(senderId);
        sms.setRequestBy(requestBy);
       // sms.setSchedule(scheduled);

        smsRepository.save(sms);

        return "success";
    }
}
