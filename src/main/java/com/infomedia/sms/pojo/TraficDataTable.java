package com.infomedia.sms.pojo;

import lombok.Data;

import java.util.Set;

@Data
public class TraficDataTable {

    private int page;

    private int totalPages;

    private int limit;

    private int lastPage;

    private Set<TrafficReport> traffics;
}
