package com.infomedia.sms.pojo;

import lombok.Data;

@Data
public class TrafficReport {

    private String id;

   // private String trxId;

    private String msisdn;

    private String senderId;

    private String message;

    private String createdDate;

    private String sentDate;

    private int messageCount;

    private String sentStatus;

    private String dlvStatus;
}
