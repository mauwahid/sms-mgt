package com.infomedia.sms.pojo;

import lombok.Data;

@Data
public class DefaultResponse {


    private int status;

    private String message;
}
