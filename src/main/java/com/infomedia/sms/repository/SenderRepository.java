package com.infomedia.sms.repository;

import com.infomedia.sms.domain.Sender;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SenderRepository extends CrudRepository<Sender, Long> {
}
