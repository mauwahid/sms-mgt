package com.infomedia.sms.controller.report;


import com.infomedia.sms.pojo.TraficDataTable;
import com.infomedia.sms.service.ReportTrafficService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/report/traffic")
public class TrafficController {

    @Autowired
    private ReportTrafficService reportTrafficService;

    @RequestMapping("/index")
    public String index() {
        return "report/traffic";
    }

    @PostMapping("/data")
    public TraficDataTable generateData(@RequestParam("date_start")String dateStart,
                                        @RequestParam("date_end")String dateEnd,
                                        @RequestParam("sender_id")String senderId,
                                        @RequestParam("status_id")String statusId,
                                        @RequestParam("page")int page){

        return reportTrafficService.generateDataReport(dateStart,dateEnd,senderId,statusId,page);
    }
}
