package com.infomedia.sms.controller.dashboard;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UserDashController {

    @RequestMapping("/dashboard")
    public String index() {
        return "dashboard/user";
    }
}
