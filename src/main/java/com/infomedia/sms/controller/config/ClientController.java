package com.infomedia.sms.controller.config;


import com.infomedia.sms.domain.Client;
import com.infomedia.sms.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/client")
public class ClientController {


    @Autowired
    private ClientService clientService;

    @RequestMapping("/index")
    public String index() {
        return "client/account/index";
    }

    @RequestMapping("/create")
    public String create() {
        return "client/account/create";
    }

    @RequestMapping("/data")
    public ResponseEntity getData() {
        Iterable<Client> clients = clientService.getAllData();
        return new ResponseEntity(clients, HttpStatus.OK);

    }


    @PostMapping("/save")
    public ResponseEntity save(@RequestParam("name")String name,
                               @RequestParam("client_type")String clientTypeId,
                               @RequestParam("billing_type")int billingType,
                               @RequestParam("address")String address,
                               @RequestParam("city")String city,
                               @RequestParam("postal")String postal,
                               @RequestParam("phone")String phone,
                               @RequestParam("email")String email,
                               @RequestParam("status")int status){

        clientService.save(name,clientTypeId, billingType, address, city, postal, phone, email, status);

        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity update(@RequestParam("id")int id,
                               @RequestParam("name")String name,
                               @RequestParam("client_type")String clientTypeId,
                               @RequestParam("billing_type")int billingType,
                               @RequestParam("address")String address,
                               @RequestParam("city")String city,
                               @RequestParam("postal")String postal,
                               @RequestParam("phone")String phone,
                               @RequestParam("email")String email,
                               @RequestParam("status")int status){

        clientService.save(name,clientTypeId, billingType, address, city, postal, phone, email, status);

        return new ResponseEntity(HttpStatus.OK);
    }


}