package com.infomedia.sms.controller.sms;

import com.infomedia.sms.service.SMSSingleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/sms/single")
public class SingleController {

    @Autowired
    private SMSSingleService smsSingleService;

    @RequestMapping("/index")
    public String index() {
        return "sms/single";
    }

    @GetMapping("/post")
    @ResponseBody
    public String postSingle(@RequestParam("msisdn")String msisdn,
                             @RequestParam("message")String message,
                             @RequestParam("request_by")String requestBy,
                             @RequestParam("sender_id")String senderId,
                             @RequestParam("scheduled")String scheduled){

        return smsSingleService.saveSMS(msisdn,message,senderId,requestBy,scheduled);
    }
}
