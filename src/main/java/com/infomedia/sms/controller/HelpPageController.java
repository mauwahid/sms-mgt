package com.infomedia.sms.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelpPageController {

    @RequestMapping("/help")
    public String index() {
        return "help";
    }
}
