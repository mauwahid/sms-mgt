package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
public class SenderGateway extends AbstactEntity{


    @Column(name = "sender_id")
    private int senderId;

    @Column
    private int axis;

    @Column
    private int esia;

    @Column
    private int flexy;

    @Column
    private int indosat;

    @Column
    private int smartfren;

    @Column
    private int telkomsel;

    @Column
    private int three;

    @Column
    private int xl;


}
