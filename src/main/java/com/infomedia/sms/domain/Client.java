package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.Set;

@Entity
@Data
public class Client extends AbstactEntity {

    @Column
    private String name;

    @Column(name = "client_type_id")
    private String clientTypeId;

    @Column
    private String address;

    @Column
    private String city;

    @Column
    private String postal;

    @Column
    private String phone;

    @Column
    private String email;

    @Column(name = "status_id")
    private int statusId;

    @Column(name = "billing_type")
    private int billingType;


    @OneToMany(mappedBy = "client",fetch = FetchType.LAZY)
    private Set<User> users;

    @OneToMany(mappedBy = "client",fetch = FetchType.LAZY)
    private Set<Sender> senders;


}
