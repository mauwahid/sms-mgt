package com.infomedia.sms.domain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class XMLMaker {

    private List<Map<String, String>> list;

    public XMLMaker() {
        list = new ArrayList<Map<String, String>>();
    }

    public void add(Map<String, String> map) {
        list.add(map);
    }

    @Override
    public String toString() {
        String s = "";

        for (int i = 0; i < list.size(); i++) {
            Map<String, String> map = list.get(i);
            Iterator it = map.keySet().iterator();
            s = s + "======================= \n";
            while (it.hasNext()) {
                String key = (String) it.next();
                String value = map.get(key);
                s = s + key + " => " + value + " | ";
            }
            s = s + "\n";
        }

        return s;
    }

    public List<Map<String, String>> getList() {
        return list;
    }
}
