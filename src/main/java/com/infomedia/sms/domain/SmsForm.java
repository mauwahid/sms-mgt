package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class SmsForm extends AbstactEntity {


    @Column
    private String msisdn;

    @Column
    private int createdBy;

    @Column
    private String requestBy;

    @Column
    private String message;

    @Column
    private Date schedule;

    @Column
    private Date dateCreated;

   }
