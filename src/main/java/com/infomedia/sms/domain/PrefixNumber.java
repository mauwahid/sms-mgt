package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class PrefixNumber extends AbstactEntity {

    @Column(name = "operator_id")
    private String operatorId;

    @Column
    private String prefix;

}
