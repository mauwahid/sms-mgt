package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Entity
public class SmsGroup extends AbstactEntity {

    @Column
    private int createdBy;
    @Column
    private String requestBy;
    @Column
    private String message;
    @Column
    private Date schedule;
    @Column
    private int status;
    @Column
    private Date dateCreated;


}
