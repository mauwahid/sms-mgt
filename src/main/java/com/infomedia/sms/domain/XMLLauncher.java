package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: herry
 * Date: 16/10/12
 * Time: 2:48 PM
 * To change this template use File | Settings | File Templates.
 */

@Data
@Entity
public class XMLLauncher extends AbstactEntity {

    @Column
    private String sender;
    @Column
    private String message;
    @Column
    private String msisdn;

}
