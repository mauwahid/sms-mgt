package com.infomedia.sms.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class SmsBulky extends AbstactEntity {

    @Column
    private int createdBy;

    @Column
    private String requestBy;

    @Column
    private String message;

    @Column
    private Date schedule;

    @Column
    private int status;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreated;


}
