package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Data
@Entity
public class SmsMaker extends AbstactEntity {

    @Column
    private int createdBy;
    @Column
    private String requestBy;
    @Column
    private String filename;
    @Column
    private String message;
    @Column
    private int status;
    @Column
    private Date schedule;
    @Column
    private Date dateCreated;

}
