package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class Recipient extends AbstactEntity {

    @Column
    private int clientId;

    @Column
    private String name;

    @Column
    private String gender;

    @Column
    private String msisdn;


}
