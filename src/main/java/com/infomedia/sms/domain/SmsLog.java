package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Data
@Entity
public class SmsLog extends AbstactEntity {

    @Column
    private int createdBy;
    @Column
    private String createdByName;
    @Column
    private String msisdn;
    @Column
    private int clientId;
    @Column
    private String clientName;
    @Column
    private String requestBy;
    @Column
    private String message;
    @Column
    private int status;
    @Column
    private String fromTable;
    @Column
    private long fromTableId;
    @Column
    private int deliveryStatus;
    @Column
    private Date dateCreated;
}
