package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Data
public class Sender extends AbstactEntity {

    @ManyToOne
    @JoinColumn
    private Client client;

    @Column
    private String user;

    @Column
    private String password;

    @Column(name = "sender_id")
    private String senderId;


}
