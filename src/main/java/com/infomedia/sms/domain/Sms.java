package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
public class Sms extends AbstactEntity
{

  @Column(name = "client_id")
  private long clientId;

  @Column
  private String msisdn;

  @Column
  private long userId;

  @Column(name = "request_by")
  private String requestBy;

  @Column
  private String message;

  @Column
  private Date schedule;

  @Column(name = "status_id")
  private int statusId;

  @Column(name = "sender_id")
  private String senderId;

  @Column
  private int sender;

  @Column(name = "trx_id")
  private String trxId;

  @Column(name = "gtw_name")
  private String gtwName;

  @Column(name = "dlv_status")
  private String dlvStatus;

  @Column(name = "dlv_date")
  private String dlvDate;
  
}
