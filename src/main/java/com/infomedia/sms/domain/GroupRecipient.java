package com.infomedia.sms.domain;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

@Entity
@Data
public class GroupRecipient extends AbstactEntity {

    @Column
    private int groupId;

    @Column
    private int recipientId;

    @Column
    @CreationTimestamp
    private Date createdDate;


}
