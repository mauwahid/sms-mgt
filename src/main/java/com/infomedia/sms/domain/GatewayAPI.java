package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Data
public class GatewayAPI  extends AbstactEntity{

    @Column
    private String name;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column
    private int port;

    @Column(name = "url_path")
    private String urlPath;


}
