package com.infomedia.sms.domain;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Data
@Entity
public class User extends AbstactEntity {

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private int userLevelId;

    @ManyToOne
    @JoinColumn
    private Client client;

    @Column
    private int userStatusId;

}

