function getLinkUrl(anchorId){

    console.log('anchorId '+anchorId);
    switch(anchorId){
        case "dashboard" :
            return "/dashboard";
            break;
        case "sms_single" :
            return "/sms/single/index";
            break;
        case "report_traffic" :
            return "/report/traffic/index";
            break;
        case "client_account" :
            return "/client/index";
            break;
        case "help" :
            return "/help";
            break;

    }

}


function loadHtmlData(href){

    $.ajax({
      url: getLinkUrl($(href).attr('href')),
      context: document.body
    }).done(function(data) {
        $( ".content-wrapper" ).html(data);
    });


}


